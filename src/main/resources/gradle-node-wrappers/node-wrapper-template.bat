@REM
@REM ============================================================================
@REM (C) Copyright Schalk W. Cronje and/or respective authors identified in
@REM documentation sections 2017-2020
@REM
@REM This software is licensed under the Apache License 2.0
@REM See http://www.apache.org/licenses/LICENSE-2.0 for license details
@REM
@REM Unless required by applicable law or agreed to in writing, software
@REM distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
@REM WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@REM See the License for the specific language governing permissions and
@REM limitations under the License.
@REM ============================================================================
@REM

@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  ~~APP_BASE_NAME~~ wrapper script for Windows
@rem
@rem ##########################################################################

@rem Relative path from this script to the directory where the Gradle wrapper
@rem might be found.
set GRADLE_WRAPPER_RELATIVE_PATH=~~GRADLE_WRAPPER_RELATIVE_PATH~~

@rem  Relative path from this script to the project cache dir (usually .gradle).
set DOT_GRADLE_RELATIVE_PATH=~~DOT_GRADLE_RELATIVE_PATH~~

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%

:init
@rem Get command-line arguments, handling Windows variants

if not "%OS%" == "Windows_NT" goto win9xME_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*

:execute
@rem Setup the command line

set APP_LOCATION_FILE=%APP_HOME%\%DOT_GRADLE_RELATIVE_PATH%\~~APP_LOCATION_FILE~~
set GRADLEW=%APP_HOME%\%GRADLE_WRAPPER_RELATIVE_PATH%\gradlew.bat

@rem If the app location is not available, set it first via Gradle
if not exist %APP_LOCATION_FILE% call :run_gradle ~~CACHE_TASK_NAME~~

@rem Read settings in from app location properties
@rem - APP_LOCATION
call %APP_LOCATION_FILE%

@rem If the app is not available, download it first via Gradle
if not exist %APP_LOCATION% call :run_gradle ~~CACHE_TASK_NAME~~

:cliconfigset

@rem Execute ~~APP_BASE_NAME~~
%APP_LOCATION% %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
exit /b 0

:run_gradle
if  exist %GRADLEW% (
    call %GRADLEW% -q --console=plain --no-daemon %*
) else (
    call gradle -q --console=plain --no-daemon %*
)
exit /b 0