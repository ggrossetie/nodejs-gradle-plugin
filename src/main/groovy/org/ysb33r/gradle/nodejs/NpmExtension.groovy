/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.internal.Downloader
import org.ysb33r.grolifant.api.v4.exec.AbstractToolExtension
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.exec.ResolveExecutableByVersion
import org.ysb33r.grolifant.api.v4.exec.ResolvedExecutableFactory

import static org.ysb33r.grolifant.api.v4.FileUtils.updateFileProperty

/** Set up global config or task-based config for NPM.
 *
 * @since 0.1
 */
@CompileStatic
class NpmExtension extends AbstractToolExtension {

    static final String NAME = 'npm'

    /** Adds the extension to the project.
     *
     * @param project Project to link to.
     */
    NpmExtension(Project project) {
        super(project)
        addResolvers(project)
        executable(this.defaultNodejs())
        this.localConfig = new File(project.rootProject.projectDir, NPM_CONFIG)
        this.globalConfig = new File(project.gradle.gradleUserHomeDir, NPM_CONFIG)
        this.homeDirectoryProperty = project.objects.property(File)
        this.homeDirectoryProperty.set(project.projectDir)
    }

    /** Adds the extension to a {@link org.ysb33r.gradle.nodejs.tasks.NpmTask} task.
     *
     * <p> Links the executable resolving to the global instance, but allows
     * it to be overriden on a per-task basis.
     *
     * @param task Task to be extended.
     */
    NpmExtension(Task task) {
        super(task, NAME)
        this.npmProjectExtensionName = NAME
        this.homeDirectoryProperty = task.project.objects.property(File)
        this.homeDirectoryProperty.set(npmExtension.homeDirectoryProvider)

        addResolvers(task.project)
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     * @param projectExtName Name of the extension that is attached to the project.
     *
     * @since 0.4
     */
    protected NpmExtension(Task task, final String projectExtName) {
        super(task, projectExtName)
        this.npmProjectExtensionName = projectExtName
        addResolvers(task.project)
    }

    /** Resolves a path to a {@code npm-cli.js} executable.
     *
     * <p> If the extension is linked to a task and not the location not configured,
     * a lookup will be performed on the project extension of the same name. This is an alias
     * method for {@code getResolvableExecutable( )}
     *
     * @return Returns the path to the located {@code npm-cli.js} executable.
     * @throw {@code ExecConfigurationException} if location was not configured.
     */
    ResolvableExecutable getResolvedNpmCliJs() {
        resolvableExecutable
    }

    /** Resolves a path to a {@code npx-cli.js} executable.
     *
     * <p> If the extension is linked to a task and not the location not configured,
     * a lookup will be performed on the project extension of the same name. This is an alias
     * method for {@code getResolvableExecutable( )}
     *
     * @return Returns the path to the located {@code npx-cli.js} executable.
     * @throw {@code ExecConfigurationException} if location was not configured.
     *
     * @since 0.9.0
     */
    ResolvableExecutable getResolvedNpxCliJs() {
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                new File(resolvableExecutable.executable.parentFile, 'npx-cli.js')
            }
        }
    }

    /** Sets NPM to be resolved from the default node.js distribution associated with this project.
     *
     * @return Something that is suitable to be passed to @{@link #executable}
     */
    @SuppressWarnings('UnnecessaryCast')
    Map<String, Object> defaultNodejs() {
        ['default': ((NodeJSExtension) (project.extensions.getByName(nodeJsExtensionName)))] as Map<String, Object>
    }

    /** Use this to configure a system path search for {@code npm}
     *
     * @return Returns a special option to be used in {@link #executable}
     */
    Map<String, Object> searchPath() {
        SEARCH_PATH
    }

    /** Location & name of global NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.gradle.gradleUserHomeDir}/npmrc"}
     *
     * @return {@link java.io.File} object pointing to global NPM config
     */
    File getGlobalConfig() {
        if (task) {
            this.globalConfig ? project.file(this.globalConfig) : npmExtension.getGlobalConfig()
        } else {
            project.file(this.globalConfig)
        }
    }

    /** Set global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setGlobalConfig(Object path) {
        this.globalConfig = path
    }

    /** Set global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void globalConfig(Object path) {
        setGlobalConfig(path)
    }

    /** Location & name of local NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.rootProject.projectDir}/npmrc"}
     *
     * @return {@link java.io.File} oject pointing to local NPM config
     */
    File getLocalConfig() {
        if (task) {
            this.localConfig ? project.file(this.localConfig) : npmExtension.getLocalConfig()
        } else {
            project.file(this.localConfig)
        }
    }

    /** Set local config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setLocalConfig(Object path) {
        this.localConfig = path
    }

    /** Set local config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void localConfig(Object path) {
        setLocalConfig(path)
    }

    /** The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     *
     */
    File getHomeDirectory() {
        homeDirectoryProvider.get()
    }

    /** The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     *
     * @since 0.8.0
     */
    Provider<File> getHomeDirectoryProvider() {
        this.homeDirectoryProperty
    }

    /** Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *   Anything that can be resolved with {@code project.file} is acceptable
     */
    void setHomeDirectory(Object homeDir) {
        updateFileProperty(project, this.homeDirectoryProperty, homeDir)
    }

    /** Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *   Anything that can be resolved with {@code project.file} is acceptable
     */
    void homeDirectory(Object homeDir) {
        homeDirectory = homeDir
    }

    /** Returns the name of the extension to look for when trying to find the Node.js extension.
     *
     * @return Name of Node.js project extension
     *
     * @since 0.4
     */
    protected String getNodeJsExtensionName() {
        NodeJSExtension.NAME
    }

    /** Returns the name of the extension to look for when trying to find the Npm extension.
     *
     * @return Name of NPM project extension or (@code null) if this a project extension.
     *
     * @since 0.4
     */
    protected String getNpmProjectExtensionName() {
        this.npmProjectExtensionName
    }

    private void addResolvers(Project project) {
        ResolveExecutableByVersion.DownloaderFactory downloaderFactory = {
            Map<String, Object> options, String version, Project p ->
                new Downloader(version, p)
        } as ResolveExecutableByVersion.DownloaderFactory

        ResolveExecutableByVersion.DownloadedExecutable resolver = { Downloader installer ->
            installer.npmExecutablePath
        } as ResolveExecutableByVersion.DownloadedExecutable

        resolverFactoryRegistry.registerExecutableKeyActions(
            new ResolveExecutableByVersion(project, downloaderFactory, resolver)
        )

        resolverFactoryRegistry.registerExecutableKeyActions('default', new ResolvedExecutableFactory() {
            @Override
            ResolvableExecutable build(Map<String, Object> options, Object from) {
                ((NodeJSExtension) from).resolvedNpmCliJs
            }
        })
    }

    private NpmExtension getNpmExtension() {
        ((NpmExtension) project.extensions.getByName(npmProjectExtensionName))
    }

    private Object localConfig
    private Object globalConfig
    private final Property<File> homeDirectoryProperty
    private final String npmProjectExtensionName

    @SuppressWarnings(['UnnecessaryCast', 'DuplicateStringLiteral'])
    private static final Map<String, Object> SEARCH_PATH = [search: 'npm'] as Map<String, Object>

    private static final String NPM_CONFIG = 'npmrc'
}