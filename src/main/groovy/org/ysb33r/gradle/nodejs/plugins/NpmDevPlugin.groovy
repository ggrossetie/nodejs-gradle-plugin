/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.bundling.Zip
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.internal.npm.PackageJson
import org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInstall
import org.ysb33r.gradle.nodejs.tasks.NpmScriptTask
import org.ysb33r.gradle.nodejs.tasks.SyncProjectToPackageJson

/** Conventions for building a project with Node and Gradle.
 *
 * <ul>
 *   <li>All Node related code goes in {@code src/node}. This includes {@code package.json}</li>
 *   <li>Tasks are added to deal with {@code package.json}
 *   <li>Wrappers can be generated a project level and will point to src in {@code src/node}
 * </ul>
 *
 * @since 0.9.0
 */
@CompileStatic
class NpmDevPlugin implements Plugin<Project> {

    public static final String DEFAULT_SRCDIR = 'src/node'
    public static final String DEFAULT_GROUP = 'Node.js'
    public static final String SCRIPT_TASK_PREFIX = 'npmRun'
    public static final String ZIP_TASK = 'nodeZip'
    public static final String INSTALL_TASK = 'packageJsonInstall'

    @Override
    void apply(Project project) {
        project.apply plugin: 'base'
        project.apply plugin: NpmPlugin

        project.extensions.getByType(NpmExtension).homeDirectory = DEFAULT_SRCDIR
        NpmPackageJsonInstall install = addPackageJsonTasks(project)
        addRuleForScriptTasks(project, install.packageJsonFileProvider)
        addZipTask(install)
    }

    private void addZipTask(NpmPackageJsonInstall install) {
        Zip nodeZip = install.project.tasks.create(ZIP_TASK, Zip)
        nodeZip.group = DEFAULT_GROUP
        nodeZip.dependsOn install
        nodeZip.from(DEFAULT_SRCDIR) { CopySpec spec ->
            spec.include 'app/**'
            spec.include 'node_modules/**'
        }
    }

    private NpmPackageJsonInstall addPackageJsonTasks(Project project) {
        NpmPackageJsonInstall install = project.tasks.create(INSTALL_TASK, NpmPackageJsonInstall)
        SyncProjectToPackageJson sync = project.tasks.create('syncProjectToPackageJson', SyncProjectToPackageJson)
        Delete clean = project.tasks.create('cleanPackageJsonInstall', Delete)

        sync.packageJsonFile = install.packageJsonFileProvider
        install.dependsOn sync
        project.tasks.getByName('assemble').dependsOn install

        clean.delete(install.extensions.getByType(NpmExtension).homeDirectoryProvider.map { File dir ->
            new File(dir, 'node_modules')
        })

        clean.group = DEFAULT_GROUP
        clean.description = 'Cleans the node_modules directory'

        install
    }

    private addRuleForScriptTasks(Project project, Provider<File> packageJsonFileProvider) {
        project.tasks.addRule('Pattern: npmRun<Script>: Runs script defined in package.json') { String requestedName ->
            if (requestedName.startsWith(SCRIPT_TASK_PREFIX)) {
                project.logger.debug("Checking whether npmRun<Script> can be applied to ${requestedName}")
                File pj = packageJsonFileProvider.get()
                if (pj.exists()) {
                    String key = requestedName.replaceFirst(SCRIPT_TASK_PREFIX, '').uncapitalize()
                    PackageJson metadata = PackageJson.parsePackageJson(pj)
                    if (metadata.scripts.containsKey(key)) {
                        NpmScriptTask run = project.tasks.create(requestedName, NpmScriptTask)
                        run.scriptName = key
                        run.mustRunAfter INSTALL_TASK
                    } else {
                        project.logger.debug("No script matches ${key}. Ignoring rule for ${requestedName}")
                    }
                } else {
                    project.logger.debug("${pj} does not exist. Ignoring rule for ${requestedName}")
                }
            }
        }
    }

}

