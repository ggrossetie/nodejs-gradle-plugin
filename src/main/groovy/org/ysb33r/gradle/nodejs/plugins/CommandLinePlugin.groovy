/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.tasks.NodeCmdlineTask
import org.ysb33r.gradle.nodejs.tasks.NpmCmdlineTask
import org.ysb33r.gradle.nodejs.tasks.NpxCmdlineTask

/** Provides comd-line access to tools use the tool name
 * as task followed by multiple --arg or single --args
 */
@CompileStatic
class CommandLinePlugin implements Plugin<org.gradle.api.Project> {
    @Override
    void apply(Project project) {
        project.apply plugin: NpmPlugin

        project.tasks.create('node', NodeCmdlineTask)
        project.tasks.create('npm', NpmCmdlineTask)
        project.tasks.create('npx', NpxCmdlineTask)
    }
}
