/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.tasks.NodeBinariesCacheTask
import org.ysb33r.gradle.nodejs.tasks.NodeWrappers

/** Provides a task for generating wrappers for node, npx
 * and npmExt.
 *
 * @since 0.9.0
 */
@CompileStatic
class WrapperPlugin implements Plugin<Project> {
    public static final String CACHE_TASK_NAME = 'cacheNodeBinaries'
    public static final String WRAPPER_TASK_NAME = 'nodeWrappers'

    @Override
    void apply(Project project) {
        project.apply plugin: NpmPlugin

        NodeBinariesCacheTask cacheTask = project.tasks.create(CACHE_TASK_NAME, NodeBinariesCacheTask)

        project.tasks.create(
            WRAPPER_TASK_NAME,
            NodeWrappers,
            cacheTask
        )
    }
}
