/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.ysb33r.grolifant.api.v4.exec.AbstractScriptExecSpec
import org.ysb33r.grolifant.api.v4.exec.AbstractToolExecSpec
import org.ysb33r.grolifant.api.v4.exec.ExternalExecutable
import org.ysb33r.grolifant.api.v4.exec.ToolExecSpec

/** Execution specification for running a Node.js script.
 *
 * @since 0.1
 */
@CompileStatic
class NodeJSExecSpec extends AbstractScriptExecSpec {

    /** Construct class and attach it to specific project.
     *
     * @param project Project this exec spec is attached to.
     * @param registry The REGISTRY to use to resolve executable.
     */
    NodeJSExecSpec(Project project, ExternalExecutable registry) {
        super(project, registry)
    }

    @Override
    AbstractToolExecSpec configure(Action<? extends ToolExecSpec> action) {
        action.execute(this)
        this
    }
}
