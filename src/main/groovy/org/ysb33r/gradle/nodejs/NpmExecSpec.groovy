/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.process.ExecSpec
import org.gradle.process.ProcessForkOptions
import org.ysb33r.gradle.nodejs.errors.UnsupportedMethodException
import org.ysb33r.grolifant.api.v4.exec.AbstractCommandExecSpec
import org.ysb33r.grolifant.api.v4.exec.AbstractToolExecSpec
import org.ysb33r.grolifant.api.v4.exec.ExternalExecutable
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.exec.ToolExecSpec

/** Specification for running an NPM command via {@code npm-cli.js}
 *
 * <p> For simplicity Gradle executes {@code npm-cli.js} directly rather
 * than use yet another indirection of the {@code npm} shell script.
 *
 * @since 0.1
 */
@CompileStatic
class NpmExecSpec extends AbstractCommandExecSpec {

    /** Construct class and attach it to specific project.
     *
     * @param project Project this exec spec is attached.
     * @param registry The registry to use to resolve NPM location.
     */
    NpmExecSpec(Project project, ExternalExecutable registry) {
        super(project, registry)
    }

    /** Do not use this tag of the method for NPM.
     *
     * @throw UnsupportedMethodException.
     */
    @Override
    ProcessForkOptions executable(Object o) {
        notifyUnsupportedMethod()
    }

    /** Do not use this tag of the method for NPM.
     *
     * @throw UnsupportedMethodException.
     */
    @Override
    void setExecutable(Object o) {
        notifyUnsupportedMethod()
    }

    /** Do not use this tag of the method for NPM.
     *
     * @throw UnsupportedMethodException.
     */
    @Override
    void setExecutable(String s) {
        notifyUnsupportedMethod()
    }

    /** Install a resolver to find {@code npm-cli.js}.
     *
     * @param resolver
     */
    @Override
    @SuppressWarnings('UnnecessaryOverridingMethod')
    void setExecutable(ResolvableExecutable resolver) {
        super.setExecutable(resolver)
    }

    /** Install a resolver to find {@code npm-cli.js}.
     *
     * @param resolver @return This object as an instance of {@link org.gradle.process.ProcessForkOptions}
     */
    @Override
    @SuppressWarnings('UnnecessaryOverridingMethod')
    ProcessForkOptions executable(ResolvableExecutable resolver) {
        super.executable(resolver)
    }

    /** Install a resolver to find the {@code node} executable.
     *
     * @param resolver
     */
    void setNodeExecutable(ResolvableExecutable resolver) {
        this.nodeExecutable = resolver
    }

    /** Install a resolver to find the {@code node} executable.
     *
     * @param resolver
     */
    void nodeExecutable(ResolvableExecutable resolver) {
        setNodeExecutable(resolver)
    }

    /** Set the a method to discover {@code npm-cli.js}
     *
     * <pre>
     *     // Use a specific tag (gradle will install the tag if need be)
     *     executable tag : '1.2.3.4'
     *
     *     // Via a physical path
     *     executable path : '/path/to/npm-cli.js'
     *
     * <pre>
     *
     * @param exe A method to find {@code npm-cli.js} as described above.
     *
     * @return This object as an instance of {@link org.gradle.process.ProcessForkOptions}.
     */
    @SuppressWarnings('UnnecessarySetter')
    void executable(Map<String, Object> exe) {
        setExecutable(exe)
    }

    /** Copies settings from this execution specification to a standard {@link org.gradle.process.ExecSpec}
     *
     * This method is intended to be called as late as possible by a project extension or a task which would want to
     * delegate to {@code project.exec} project extension. It will cause arguments to be evaluated.
     * The only items not immediately evaluated are {@code workingDir} and {@code executable}.
     *
     * @param execSpec Exec spec to configure.
     */
    @Override
    void copyToExecSpec(ExecSpec execSpec) {
        super.copyToExecSpec(execSpec)
        execSpec.executable = nodeExecutable.executable.absolutePath
    }

    /**
     * Configure this spec from an {@link org.gradle.api.Action}
     *
     * @param action Configuration action.
     * @return {@code this}.
     */
    @Override
    AbstractToolExecSpec configure(Action<? extends ToolExecSpec> action) {
        action.execute(this)
        this
    }

    @Override
    List<String> getExeArgs() {
        super.exeArgs + ['--scripts-prepend-node-path=true']
    }

    /** Builds up the command-line.
     *
     * @return list of built-up arguments.
     *
     * @throw {@code GradleException} if executable is not set.
     */
    @Override
    protected List<String> buildCommandLine() {
        List<String> finalCmdLine = [
            nodeExecutable.executable.absolutePath,
        ]

        finalCmdLine.addAll super.buildCommandLine()
        finalCmdLine
    }

    private void notifyUnsupportedMethod() {
        throw new UnsupportedMethodException('''Use the Map tag to set the executable''')
    }

    private ResolvableExecutable nodeExecutable

}
