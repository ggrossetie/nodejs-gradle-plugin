/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.pkgwrapper

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.ExtensionContainer
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant.api.v4.AbstractCombinedProjectTaskExtension
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.exec.ResolverFactoryRegistry

/** A base class that can be used to wrap specific NPM packages as features in Gradle,
 *   thereby allowing for configuraton both a project or task level.
 *
 * @since 0.1
 */
@CompileStatic
abstract class AbstractPackageWrappingExtension extends AbstractCombinedProjectTaskExtension {

    /** Sets wrapper executable.
     *
     * It can be passed by a single map option.
     *
     * <code>
     *   // By tag (Gradle will download and cache the correct distribution).
     *   executable version : '3.9.1'
     *
     *   // By a physical path (
     *   executable path : '/path/to/executable-file.js'
     * </code>
     *
     * @param opts Map taking one of the keys or methods mentioned above.
     */
    void executable(final Map<String, Object> opts) {
        this.resolvableExecutable = resolverFactoryRegistry.getResolvableExecutable(opts)
    }

    /** Lazy-evaluated location of the wrapped executable entry point.
     *
     * @return Resolved executable for the specific executable.
     * @deprecated Use{@code getResolvableExecutable} instead.
     */
    ResolvableExecutable getResolvedExecutable() {
        getResolvableExecutable()
    }

    /** Lazy-evaluated location of the wrapped executable entry point.
     *
     * @return Resolved executable for the specific executable.
     */
    @SuppressWarnings('LineLength')
    ResolvableExecutable getResolvableExecutable() {
        if (this.task) {
            this.resolvableExecutable ?: ((AbstractPackageWrappingExtension) (project.extensions.getByName(extensionName))).resolvableExecutable
        } else {
            this.resolvableExecutable
        }
    }

    /** Sets the installation group (production, development, optional).
     *
     * @param installGroup
     */
    void setInstallGroup(NpmDependencyGroup installGroup) {
        this.installGroup = installGroup
    }

    /** The installation group for this executable.
     *
     * @return The configured instalaltion group.
     *   If this is a task extension and it has not been configured, it will query the equivalent project extension.
     *   For a project extension that is not configured, {@code NpmDependencyGroup.PRODUCTION} will be returned
     */
    @SuppressWarnings('LineLength')
    NpmDependencyGroup getInstallGroup() {
        if (this.task) {
            this.installGroup ?: ((AbstractPackageWrappingExtension) (project.extensions.getByName(extensionName))).installGroup
        } else {
            this.installGroup
        }
    }
    /** Associate extension with a project.
     *
     * Defaults to {@link NpmDependencyGroup.PRODUCTION}.
     *
     * @param project Associated project
     * @param packageName Name of package
     */
    protected AbstractPackageWrappingExtension(Project project, String packageName) {
        super(project)
        this.registry = new ResolverFactoryRegistry(project)
        addTagResolver(null, packageName, project)
        this.installGroup = NpmDependencyGroup.PRODUCTION
    }

    /** Associate extension with a project.
     *
     * @param project
     */
    protected AbstractPackageWrappingExtension(Project project, String packageName, NpmDependencyGroup installGroup) {
        super(project)
        this.registry = new ResolverFactoryRegistry(project)
        addTagResolver(null, packageName, project)
        this.installGroup = installGroup
    }

    /** Associate extension with a task.
     *
     * @param task
     * @param name Name by which project extension is known.
     */
    protected AbstractPackageWrappingExtension(Task task, final String name) {
        super(task, name)
    }

    /** Access to the registry of executable resolver factories.
     *
     * <p> This is used to add additional factory types i.e. for version-based resolving.
     *
     * @return Registry
     */
    protected ResolverFactoryRegistry getResolverFactoryRegistry() {
        this.registry ?: ((AbstractPackageWrappingExtension) projectExtension).resolverFactoryRegistry
    }

    /** {@link NpmExtension} associated with the project or task extension
     *
     * @return {@link NpmExtension}
     */
    protected NpmExtension getNpmExtension() {
        ExtensionContainer ext = this.task ? this.task.extensions : this.project.extensions
        (NpmExtension) (ext.getByName(NpmExtension.NAME))
    }

    /** {@link NodeJSExtension} associated with the project or task extension
     *
     * @return {@link NodeJSExtension}
     */
    protected NodeJSExtension getNodeJSExtension() {
        ExtensionContainer ext = this.task ? this.task.extensions : this.project.extensions
        (NodeJSExtension) (ext.getByName(NodeJSExtension.NAME))
    }

    /** The entrypoint path relative to the installed executable folder
     *
     * @return Relative path
     */
    abstract protected String getEntryPoint()

    /** Returns the name by which the extension is known.
     *
     * @return Extension name. Never null.
     */
    abstract protected String getExtensionName()

    private void addTagResolver(final String scope, final String packageName, Project project) {
        TagBasedPackageResolver.EntryPoint entryPointLocator = {
            this.entryPoint
        } as TagBasedPackageResolver.EntryPoint

        TagBasedPackageResolver.InstallGroup installGroupLocator = {
            this.getInstallGroup()
        } as TagBasedPackageResolver.InstallGroup

        resolverFactoryRegistry.registerExecutableKeyActions(
            new TagBasedPackageResolver(
                scope,
                packageName,
                project,
                nodeJSExtension,
                npmExtension,
                installGroupLocator,
                entryPointLocator
            )
        )
    }

    private final ResolverFactoryRegistry registry
    private NpmDependencyGroup installGroup
    private ResolvableExecutable resolvableExecutable
}
