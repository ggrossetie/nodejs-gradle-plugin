/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.pkgwrapper

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.SimpleNpmPackageDescriptor
import org.ysb33r.gradle.nodejs.internal.npm.NpmPackageResolver
import org.ysb33r.grolifant.api.v4.StringUtils
import org.ysb33r.grolifant.api.v4.exec.NamedResolvedExecutableFactory
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.exec.ResolverFactoryRegistry

/** A base class for creating resolvers for specific NPM packages.
 *
 * <p> This class is meant to be used when creating an ecosystem for specific broad-use
 * NPM-based tools such as Gulp.
 *
 * @since 0.2
 */
@CompileStatic
class TagBasedPackageResolver implements NamedResolvedExecutableFactory {

    /** Obtains the installation group for a package
     *
     */
    static interface InstallGroup {
        NpmDependencyGroup getGroup()
    }

    /** Obtains the entry point for package.
     *
     */
    static interface EntryPoint {
        String getEntryPoint()
    }

    /**
     *
     * @param scope Resolve scope. Can be {@code null}.
     * @param packageName The name of the package that will be resolved.
     * @param project Project this is associated with
     * @param nodeExtension Node extension that should be used for resolving Node questions.
     * @param npmExtension NPM extension that shgoul dbe used for resolving NPM questions.
     * @param installGroupLocator Ability to obtain the installation group for a package when package needs to
     *   be resolved.
     * @param entryPointLocator Ability to obtain the entry point for a package when package needs to be resolved.
     */
    @SuppressWarnings('ParameterCount')
    TagBasedPackageResolver(
        final String scope,
        final String packageName,
        Project project,
        NodeJSExtension nodeExtension,
        NpmExtension npmExtension,
        InstallGroup installGroupLocator,
        EntryPoint entryPointLocator
    ) {
        this.scope = scope
        this.packageName = packageName
        this.groupLocator = installGroupLocator
        this.entryPointLocator = entryPointLocator
        this.npmPackageResolver = new NpmPackageResolver(project, nodeExtension, npmExtension)
    }

    /** Returns a name that can be used as a key into a {@link ResolverFactoryRegistry}.
     *
     * Always return 'version'.
     */
    final String name = 'version'

    @Override
    ResolvableExecutable build(Map<String, Object> options, Object from) {
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                SimpleNpmPackageDescriptor descriptor = new SimpleNpmPackageDescriptor(
                    scope,
                    packageName,
                    StringUtils.stringize(from)
                )
                npmPackageResolver.resolvesWithEntryPoint(descriptor, entryPointLocator.entryPoint, groupLocator.group)
            }
        }
    }

    private final String scope
    private final String packageName
    private final InstallGroup groupLocator
    private final EntryPoint entryPointLocator
    private final NpmPackageResolver npmPackageResolver
}
