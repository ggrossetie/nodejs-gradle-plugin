/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.Transformer
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.internal.Downloader
import org.ysb33r.grolifant.api.OperatingSystem
import org.ysb33r.grolifant.api.v4.MapUtils
import org.ysb33r.grolifant.api.v4.exec.AbstractToolExtension
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.exec.ResolveExecutableByVersion

import java.util.concurrent.Callable

import static org.ysb33r.gradle.nodejs.internal.Downloader.OS
import static org.ysb33r.gradle.nodejs.utils.NodeJSExecutor.defaultEnvironment
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/** Configure project defaults or task specifics for Node.js.
 *
 * @since 0.1
 */
@CompileStatic
class NodeJSExtension extends AbstractToolExtension {

    static final String NAME = 'nodejs'

    /** The default version of Node.js that will be used on
     * a supported platform if nothing else is configured.
     */
    static final String NODEJS_DEFAULT = '12.16.3'

    /** Constructs a new extension which is attached to the provided project.
     *
     * @param project Project this extensionm is associated with.
     */
    @SuppressWarnings('UnnecessaryCast')
    NodeJSExtension(Project project) {
        super(project)
        addVersionResolver(project)
        executable([version: NODEJS_DEFAULT] as Map<String, Object>)
        this.env = [:]
        this.env.putAll(defaultEnvironment)

        prefixPath(project.provider({ ->
            resolvableExecutable.executable.parentFile.canonicalPath
        } as Callable<String>))
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     */
    NodeJSExtension(Task task) {
        super(task, NAME)
        addVersionResolver(task.project)
        this.env = [:]
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     * @param projectExtName Name of the extension that is attached to the project.
     *
     * @since 0.4
     */
    protected NodeJSExtension(Task task, final String projectExtName) {
        super(task, projectExtName)
        addVersionResolver(task.project)
    }

    /** Resolves a path to a {@code node} executable.
     *
     * @return Returns the path to the located {@code node} executable.
     * @throw {@code ExecConfigurationException} if executable was not configured.
     */
    ResolvableExecutable getResolvableNodeExecutable() {
        super.resolvableExecutable
    }

    /** Resolves a path to a {@code npm} executable that is associated with the configured Node.js.
     *
     * @return Returns the path to the located {@code npm} executable.
     * @throw {@code ExecConfigurationException} if executable was not configured.
     */
    ResolvableExecutable getResolvedNpmCliJs() {
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                File root
                if (OperatingSystem.current().windows) {
                    root = resolvableExecutable.executable.parentFile
                } else {
                    root = new File(resolvableExecutable.executable.parentFile.parentFile, 'lib')
                }
                new File(root, 'node_modules/npm/bin/npm-cli.js')
            }
        }
    }

    /** Use this to configure a system path search for Node
     *
     * @return Returns a special option to be used in {@link #executable}
     */
    static Map<String, Object> searchPath() {
        this.SEARCH_PATH
    }

    /** Replace current environment with new one.
     * If this is called on the task extension, no project extension environment will
     * be used.
     *
     * @param args New environment key-value map of properties.
     *
     * @since 0.7
     */
    void setEnvironment(Map<String, ?> args) {
        if (task) {
            taskOnlyEnvironment = true
        }
        this.env.clear()
        this.env.putAll((Map<String, Object>) args)
    }

    /** Environment for running the exe
     *
     * <p> Calling this will resolve all lazy-values in the variable map.
     *
     * @return Map of environmental variables that will be passed.
     *
     * @since 0.7
     */
    Map<String, String> getEnvironment() {
        if (task && taskOnlyEnvironment) {
            MapUtils.stringizeValues(this.env)
        } else if (task) {
            projExt.environment + MapUtils.stringizeValues(this.env)
        } else {
            MapUtils.stringizeValues(this.env)
        }
    }

    /** Add environmental variables to be passed to the exe.
     *
     * @param args Environmental variable key-value map.
     *
     * @since 0.7
     */
    void environment(Map<String, ?> args) {
        this.env.putAll((Map<String, Object>) args)
    }

    /** Adds the system path to the execution environment.
     *
     * @since 0.7
     */
    void useSystemPath() {
        appendPath(project.provider { -> System.getenv(OS.pathVar) })
    }

    /** Add search to system path
     *
     * In the case of a task this will be prefixed to both the task and project extension's version
     * of the system path.
     *
     * @param prefix Provider of a path item that can be prefixed to the current system path.
     *
     * @since 0.9.2
     */
    void prefixPath(Provider<String> prefix) {
        Object current = this.env[OS.pathVar]
        if (task && taskOnlyEnvironment) {
            if (current == null) {
                this.env[OS.pathVar] = prefix
            } else {
                this.env[OS.pathVar] = prefix.map { String s -> "${s}${OS.pathSeparator}${stringize(current)}" }
            }
        } else if (task) {
            Provider<String> combinator
            NodeJSExtension parent = projExt
            if (current == null) {
                combinator = prefix.map({ String s ->
                    "${s}${OS.pathSeparator}${stringize(parent.env[OS.pathVar])}"
                } as Transformer<String, String>)
            } else {
                combinator = prefix.map({ String s ->
                    "${s}${OS.pathSeparator}${stringize(current)}"
                } as Transformer<String, String>)
            }
            this.env[OS.pathVar] = combinator
        } else {
            if (current == null) {
                this.env[OS.pathVar] = prefix
            } else {
                this.env[OS.pathVar] = prefix.map { String s -> "${s}${OS.pathSeparator}${stringize(current)}" }
            }
        }
    }

    /** Add search to system path.
     *
     * In the case of a task this will be appended to both the task and project extension's version
     * of the system path.
     *
     * @param postfix Provider of a path item that can be appended to the current system path.
     *
     * @since 0.9.2
     */
    void appendPath(Provider<String> postfix) {
        Object current = this.env[OS.pathVar]
        if (task && taskOnlyEnvironment) {
            if (current == null) {
                this.env[OS.pathVar] = postfix
            } else {
                this.env[OS.pathVar] = postfix.map { String s -> "${stringize(current)}${OS.pathSeparator}${s}" }
            }
        } else if (task) {
            Provider<String> combinator
            NodeJSExtension parent = projExt
            if (current == null) {
                combinator = postfix.map({ String s ->
                    "${stringize(parent.env[OS.pathVar])}${OS.pathSeparator}${s}"
                } as Transformer<String, String>)
            } else {
                combinator = postfix.map({ String s ->
                    "${stringize(current)}${OS.pathSeparator}${s}"
                } as Transformer<String, String>)
            }
            this.env[OS.pathVar] = combinator
        } else {
            if (current == null) {
                this.env[OS.pathVar] = postfix
            } else {
                this.env[OS.pathVar] = postfix.map { String s -> "${stringize(current)}${OS.pathSeparator}${s}" }
            }
        }
    }

    private void addVersionResolver(Project project) {
        ResolveExecutableByVersion.DownloaderFactory downloaderFactory = {
            Map<String, Object> options, String version, Project p ->
                new Downloader(version, p)
        } as ResolveExecutableByVersion.DownloaderFactory

        ResolveExecutableByVersion.DownloadedExecutable resolver = { Downloader installer ->
            installer.nodeExecutablePath
        } as ResolveExecutableByVersion.DownloadedExecutable

        resolverFactoryRegistry.registerExecutableKeyActions(
            new ResolveExecutableByVersion(project, downloaderFactory, resolver)
        )
    }

    private NodeJSExtension getProjExt() {
        (NodeJSExtension) projectExtension
    }

    @SuppressWarnings('UnnecessaryCast')
    private static final Map<String, Object> SEARCH_PATH = [search: 'node'] as Map<String, Object>
    private static final OperatingSystem OS = OperatingSystem.current()
    private final Map<String, Object> env = [:]
    private boolean taskOnlyEnvironment = false
}
