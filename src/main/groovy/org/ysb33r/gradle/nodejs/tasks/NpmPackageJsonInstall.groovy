/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.TaskAction

import static org.gradle.api.tasks.PathSensitivity.RELATIVE
import static org.ysb33r.gradle.nodejs.plugins.NpmDevPlugin.DEFAULT_GROUP
import static org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor.installPackagesFromDescription

/** Installs the packages as described by a {@code package.json} file
 *
 * @since 0.1
 */
class NpmPackageJsonInstall extends AbstractNodeBaseTask {

    NpmPackageJsonInstall() {
        super()
        group = DEFAULT_GROUP
        this.packageJson = project.objects.property(File)
        this.packageJson.set(npmExtension.homeDirectoryProvider.map { File file ->
            new File(file, 'package.json')
        })
        this.lockFile = project.objects.property(File)
        this.lockFile.set(this.packageJson.map { File file ->
            new File(file.parentFile, 'executable-lock.json')
        })
    }

    /** Set to {@code true} if only production dependencies should be installed.
     *
     * @since 0.9.0
     */
    @Internal
    boolean productionOnly = false

    /** The package.json file that this task will query.
     *
     * @return File object.
     */
    @Internal
    File getPackageJsonFile() {
        packageJsonFileProvider.get()
    }

    /** The package.json file that this task will query.
     *
     * @return File provider object.
     *
     * @since 0.8.0
     */
    @PathSensitive(RELATIVE)
    @InputFile
    Provider<File> getPackageJsonFileProvider() {
        this.packageJson
    }

    /** The executable-lock.json file that this task will create.
     *
     * @return File object.
     */
    @Internal
    File getPackageLockFile() {
        packageLockFileProvider.get()
    }

    /** The executable-lock.json file that this task will create.
     *
     * @return File object.
     *
     * @since 0.8.0
     */
    @OutputFile
    Provider<File> getPackageLockFileProvider() {
        this.lockFile
    }

    /** Replace list of arguments with a new list.
     *
     * @param args new arguments to use.
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    void setAdditionalInstallArgs(Iterable<String> args) {
        this.additionalInstallArgs.clear()
        this.additionalInstallArgs.addAll(args)
    }

    /** Adds more installation arguments
     *
     * @param args One or more arguments
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    void additionalInstallArgs(String... args) {
        additionalInstallArgs(args as List)
    }

    /** Adds more installation arguments
     *
     * @param args Iterable list of arguments
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    void additionalInstallArgs(Iterable<String> args) {
        this.additionalInstallArgs.addAll(args)
    }

    /** Customise installation via additional argument that are passed to {@code npm install}.
     *
     * @return List of additional arguments.
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    @Input
    Iterable<String> getAdditionalInstallArgs() {
        this.additionalInstallArgs
    }

    @TaskAction
    void exec() {
        installPackagesFromDescription(
            project,
            nodeExtension,
            npmExtension,
            packageJsonFile,
            this.additionalInstallArgs,
            productionOnly
        )
    }

    private final Property<File> lockFile
    private final Property<File> packageJson
    private final List<String> additionalInstallArgs = []
}
