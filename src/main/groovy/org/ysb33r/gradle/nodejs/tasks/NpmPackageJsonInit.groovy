/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.PACKAGE_JSON
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.formatName
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.formatVersion
import static org.ysb33r.gradle.nodejs.plugins.NpmDevPlugin.DEFAULT_GROUP
import static org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor.initPkgJson
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/** Create a {@code package.json} file if it does not exist.
 *
 * @since 0.8.0
 */
@CompileStatic
class NpmPackageJsonInit extends AbstractNodeBaseTask {

    NpmPackageJsonInit() {
        super()
        group = DEFAULT_GROUP
        this.packageJson = project.objects.property(File)
        this.packageJson.set(npmExtension.homeDirectoryProvider.map { File file ->
            new File(file, PACKAGE_JSON)
        })
    }

    /** The package.json file that this task will create update.
     *
     * @return File object.
     */
    @Internal
    File getPackageJsonFile() {
        packageJsonFileProvider.get()
    }

    /** The package.json file that this task will create update.
     *
     * @return File object.
     */
    @OutputFile
    Provider<File> getPackageJsonFileProvider() {
        this.packageJson
    }

    @TaskAction
    void exec() {
        File packageFile = packageJsonFileProvider.get()
        if (!packageFile.exists()) {
            initPkgJson(
                formatName(project.name),
                formatVersion(stringize(project.version), true),
                project,
                nodeExtension,
                npmExtension
            )
        }
    }

    private final Property<File> packageJson
}
