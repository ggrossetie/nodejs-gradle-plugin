/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.internal.Transform
import org.ysb33r.grolifant.api.v4.wrapper.script.AbstractScriptWrapperTask

import javax.inject.Inject

@CompileStatic
class NodeWrappers extends AbstractScriptWrapperTask {
    @Inject
    NodeWrappers(NodeBinariesCacheTask cacheTask) {
        super()
        this.cacheTask = cacheTask
        inputs.file(cacheTask.locationPropertiesFile)
        dependsOn(cacheTask)
        this.npmExt = project.extensions.getByType(NpmExtension)
        useWrapperTemplatesInResources(
            '/gradle-node-wrappers',
            MAPPING
        )
        outputs.files(Transform.toList(MAPPING.values()) {
            new File(project.projectDir, it)
        })
    }

    @Override
    protected String getBeginToken() {
        '~~'
    }

    @Override
    protected String getEndToken() {
        '~~'
    }

    @Override
    protected Map<String, String> getTokenValuesAsMap() {
        [
            APP_BASE_NAME               : 'node',
            GRADLE_WRAPPER_RELATIVE_PATH: project.relativePath(project.rootDir),
            DOT_GRADLE_RELATIVE_PATH    : project.relativePath(cacheTask.locationPropertiesFile.get().parentFile),
            APP_LOCATION_FILE           : cacheTask.locationPropertiesFile.get().name,
            CACHE_TASK_NAME             : project.absoluteProjectPath(cacheTask.name),
        ]
    }

    private static final Map<String, String> MAPPING = [
        'node-wrapper-template.sh' : 'nodew',
        'node-wrapper-template.bat': 'nodew.bat',
        'npm-wrapper-template.sh'  : 'npmw',
        'npm-wrapper-template.bat' : 'npmw.bat',
        'npx-wrapper-template.sh'  : 'npxw',
        'npx-wrapper-template.bat' : 'npxw.bat'
    ]

    private final NodeBinariesCacheTask cacheTask
    private final NpmExtension npmExt
}
