/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Transformer
import org.gradle.api.artifacts.Configuration
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.dependencies.npm.NpmSelfResolvingDependency
import org.ysb33r.gradle.nodejs.internal.Transform
import org.ysb33r.gradle.nodejs.internal.npm.PackageJson
import org.ysb33r.grolifant.api.v4.FileUtils

import java.util.concurrent.Callable

import static org.ysb33r.gradle.nodejs.NpmDependencyGroup.DEVELOPMENT
import static org.ysb33r.gradle.nodejs.NpmDependencyGroup.OPTIONAL
import static org.ysb33r.gradle.nodejs.NpmDependencyGroup.PRODUCTION
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.Indentation.FOUR_SPACES
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.Indentation.TWO_SPACES
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.formatName
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.formatVersion
import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.parsePackageJson
import static org.ysb33r.gradle.nodejs.plugins.NpmDevPlugin.DEFAULT_GROUP
import static org.ysb33r.grolifant.api.v4.FileUtils.updateFileProperty
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize
import static org.ysb33r.grolifant.api.v4.StringUtils.updateStringProperty

/** Task that will sync the project version into the {@code package.json} file.
 *
 * @since 0.8.0
 */
@CompileStatic
class SyncProjectToPackageJson extends DefaultTask {

    static enum PackageConflict {
        PACKAGE_JSON,
        GRADLE
    }

    public static final PackageConflict PACKAGE_JSON = PackageConflict.PACKAGE_JSON
    public static final PackageConflict GRADLE = PackageConflict.GRADLE

    SyncProjectToPackageJson() {
        group = DEFAULT_GROUP
        description = 'Updates version inside package.json'

        this.configurationProvider = project.provider({
            Transform.toList(this.configurations) {
                it instanceof Configuration ? it : project.configurations.getByName(stringize(it))
            }
        } as Callable<List<Configuration>>)

        this.packageJson = project.objects.property(File)

        this.packageJsonVersion = project.objects.property(String)
        this.packageJsonName = project.objects.property(String)

        setPackageJsonName({ Project project -> project.name }.curry(project))
        setPackageJsonVersion({ Project project -> project.version }.curry(project))

        this.configurationNames = this.configurationProvider.map(
            new Transformer<List<String>, List<Configuration>>() {
                @Override
                List<String> transform(List<Configuration> configurations) {
                    configurations*.name
                }
            })
    }

    /** Force NPM semantic versioning to be used.
     *
     * If project.version is {@code 1.0} then {@code 1.0.0} will used as the version for {@code package.json}.
     * Like if the version is {@code 1.0-alpha1} then {@code 1.0.0-alpha1} will be used.
     *
     * Semantic versioning is forced by default.
     */
    @Input
    boolean forceSemver = true

    /** Whether to force a two-space indent when rewriting the file.
     *
     */
    @Internal
    boolean forceTwoSpaceIndent = false

    /** Sorts the output keys.
     *
     * Mimics the functionality of {@link https://www.npmjs.com/package/sort-package-json}. Default is to not sort.
     */
    @Internal
    boolean sortOutput = false

    /** If there are version differences between the dependencies specified in {@code package.json} and the
     * {@code dependencies} block, specify which system has precedence. By default is is Gradle.
     */
    @Internal
    PackageConflict conflictMode = PackageConflict.GRADLE

    /**
     * List of configuration names that must be searched for NPM dependencies
     */
    @Input
    final Provider<List<String>> configurationNames

    /** Set the version for {@code package.json} file. If not set, it will default
     * to {@code project.version}.
     *
     * @param ver Anything convertible to a string using {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize}.
     */
    void setPackageJsonVersion(Object ver) {
        updateStringProperty(project, this.packageJsonVersion, { SyncProjectToPackageJson task ->
            formatVersion(stringize(ver), task.forceSemver)
        }.curry(this) as Callable<String>)
    }

    /** Version that will be use for {@code package.json}
     *
     * @return Version string
     */
    @Input
    Provider<String> getPackageJsonVersion() {
        this.packageJsonVersion
    }

    /** Set the name for {@code package.json} file. If not set it will default
     * to a NPM-safe {@code project.name}.
     *
     * @param projName Anything convertible to a string using {@link org.ysb33r.grolifant.api.v4.StringUtils#stringize}.
     *
     * @since 0.9.0
     */
    void setPackageJsonName(Object projName) {
        updateStringProperty(project, this.packageJsonName, { ->
            formatName(stringize(projName))
        } as Callable<String>)
    }

    /** Project name that will be use for {@code package.json}
     *
     * @return Version string
     *
     * @since 0.9.0
     */
    @Input
    Provider<String> getPackageJsonName() {
        this.packageJsonName
    }

    /** Set the location of the {@code package.json} file
     *
     * @param file Anything convertible to a {@link java.io.File} using {@link FileUtils#fileize}
     */
    void setPackageJsonFile(Object file) {
        updateFileProperty(project, this.packageJson, file)
    }

    /** Location of the {@code package.json} file.
     *
     * @return Provider to the location of {@code package.json}
     */
    @OutputFile
    Provider<File> getPackageJsonFile() {
        this.packageJson
    }

    /** Adds configurations which contain NPM dependencies.
     *
     * These dependencies will added to the correct scope in the {@code package.json} file.
     *
     * @param configs Configurations instances or anything that can be resolved to a configuration name.
     */
    void npmConfigurations(Object... configs) {
        configurations.addAll(configs)
    }

    @TaskAction
    void exec() {
        def configs = configurationProvider.get()
        File packageFile = packageJsonFile.get()

        List<NpmSelfResolvingDependency> deps = configs.empty ? [] : findNpmDependencies(configs)
        PackageJson pj = parsePackageJson(packageFile)
        pj.version = packageJsonVersion.get()
        pj.name = packageJsonName.get()
        if (!deps.empty) {
            updateDependencies(pj.dependencies, deps, PRODUCTION)
            updateDependencies(pj.devDependencies, deps, DEVELOPMENT)
            updateDependencies(pj.optionalDependencies, deps, OPTIONAL)
        }
        pj.writeToFile(packageFile, forceTwoSpaceIndent ? TWO_SPACES : FOUR_SPACES, sortOutput)
    }

    private void updateDependencies(
        Map<String, String> depMap,
        List<NpmSelfResolvingDependency> deps,
        NpmDependencyGroup group
    ) {
        Map<String, String> filteredDeps = findDeps(deps, group)
        if (!filteredDeps.isEmpty()) {
            if (conflictMode == GRADLE) {
                depMap.putAll(filteredDeps)
            } else {
                filteredDeps.each { k, v ->
                    depMap.putIfAbsent(k, v)
                }
            }
        }
    }

    private Map<String, String> findDeps(List<NpmSelfResolvingDependency> deps, NpmDependencyGroup group) {
        deps.findAll {
            it.installGroup == group
        }.collectEntries {
            String key = it.scope ? "@${it.scope}/${it.name}" : it.name
            [key, it.version]
        }
    }

    private List<NpmSelfResolvingDependency> findNpmDependencies(List<Configuration> configurations) {
        final List<NpmSelfResolvingDependency> deps = []
        for (Configuration cfg : configurations) {
            deps.addAll(cfg.allDependencies.findAll {
                it instanceof NpmSelfResolvingDependency
            } as List<NpmSelfResolvingDependency>)
        }

        deps
    }

    private final Property<String> packageJsonVersion
    private final Property<String> packageJsonName
    private final Property<File> packageJson
    private final Provider<List<Configuration>> configurationProvider
    private final List<Object> configurations = []
}
