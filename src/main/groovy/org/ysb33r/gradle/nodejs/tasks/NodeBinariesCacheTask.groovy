/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant.api.v4.wrapper.script.AbstractCacheBinaryTask

import static org.ysb33r.gradle.nodejs.internal.Downloader.OS

/** Caches node binaries
 *
 * @since 0.9.0
 */
@CompileStatic
class NodeBinariesCacheTask extends AbstractCacheBinaryTask {
    public static final String PROPERTIES_FILENAME = OS.windows ? 'node-wrapper.bat' : 'node-wrapper.properties'

    NodeBinariesCacheTask() {
        super(PROPERTIES_FILENAME)
        this.nodejs = project.extensions.getByType(NodeJSExtension)
        this.npm = project.extensions.getByType(NpmExtension)
    }

    @Override
    protected String getBinaryVersion() {
        switch (nodejs.resolvableExecutableType.type) {
            case 'version':
                return nodejs.resolvableExecutableType.value.get()
            default:
                ''
        }
    }

    @Override
    protected String getBinaryLocation() {
        nodejs.resolvableExecutable.executable.canonicalPath
    }

    @Override
    protected String getPropertiesDescription() {
        "Describes node/npm/npx usages for the ${project.name} project"
    }

    @Override
    protected Map<String, String> getAdditionalProperties() {
        super.additionalProperties + [
            NPM_LOCATION           : npm.resolvedNpmCliJs.executable.canonicalPath,
            NPX_LOCATION           : npm.resolvedNpxCliJs.executable.canonicalPath,
            NPM_CONFIG_USERCONFIG  : npm.localConfig.absolutePath,
            NPM_CONFIG_GLOBALCONFIG: npm.globalConfig.absolutePath
        ]
    }

    private final NodeJSExtension nodejs
    private final NpmExtension npm
}