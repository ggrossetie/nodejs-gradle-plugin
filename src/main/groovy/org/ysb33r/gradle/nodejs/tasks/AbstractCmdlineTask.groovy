/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.apache.commons.text.StringTokenizer
import org.apache.commons.text.matcher.StringMatcherFactory
import org.gradle.api.Action
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.plugins.NpmDevPlugin

import java.util.concurrent.Callable

import static org.ysb33r.grolifant.api.v4.FileUtils.updateFileProperty
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize
import static org.ysb33r.grolifant.api.v4.StringUtils.updateStringProperty

@CompileStatic
class AbstractCmdlineTask extends DefaultTask {

    /** By default this task is never up to date. Setting this value to {@code true} gives it the option to be
     * up to date.
     */
    @Internal
    boolean canBeUpToDate = false

    @Option(option = 'arg', description = 'Command-line option (Can be repeated)')
    void setArgs(List<String> args) {
        this.args.addAll(args)
    }

    @Option(option = 'args', description = 'Command-line options (space separated). Applied before single arguments.')
    void setArgsLine(String args) {
        StringTokenizer st = new StringTokenizer(args)
        st.quoteMatcher = StringMatcherFactory.INSTANCE.quoteMatcher()
        this.args.addAll(st.tokenList)
    }

    @Input
    List<String> getFinalArgs() {
        stringize(this.args)
    }

    void setCommandProvider(Object cmdProvider) {
        updateStringProperty(project, this.cmdProvider, cmdProvider)
    }

    void setEnvironmentProvider(Provider<Map<String, Object>> env) {
        this.envProvider = env
    }

    void setWorkingDir(Object dir) {
        updateFileProperty(project, this.workingDirProvider, dir)
    }

    @TaskAction
    void exec() {
        String cmd = this.cmdProvider.get()
        Map<String, Object> env = this.envProvider.get()
        File wd = this.workingDirProvider.get()
        project.exec(new Action<ExecSpec>() {
            @Override
            void execute(ExecSpec execSpec) {
                execSpec.environment = env
                execSpec.executable = cmd
                execSpec.args = finalArgs
                execSpec.workingDir = wd
            }
        })
    }

    protected AbstractCmdlineTask() {
        npm = extensions.create(NpmExtension.NAME, NpmExtension, this)
        nodejs = extensions.create(NodeJSExtension.NAME, NodeJSExtension, this)
        this.cmdProvider = project.objects.property(String)
        this.workingDirProvider = project.objects.property(File)
        this.envProvider = project.provider({ -> [:] } as Callable<Map<String, Object>>)

        group = NpmDevPlugin.DEFAULT_GROUP
        workingDir = npm.homeDirectoryProvider

        outputs.upToDateWhen { AbstractCmdlineTask task -> task.canBeUpToDate }
    }

    @Internal
    protected final NpmExtension npm

    @Internal
    protected final NodeJSExtension nodejs

    /** Allows the derived task to push additional arguments that will usually precede any arguments derived from
     * command-line.
     *
     * @param args One or more args.
     */
    protected void addArgs(Object... args) {
        this.args.addAll(args)
    }

    private final Property<String> cmdProvider
    private final Property<File> workingDirProvider
    private final List<Object> args = []
    private Provider<Map<String, Object>> envProvider
}
