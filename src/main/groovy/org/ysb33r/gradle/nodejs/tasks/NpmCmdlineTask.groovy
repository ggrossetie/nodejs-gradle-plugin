/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.ysb33r.gradle.nodejs.NpmExtension

import java.util.concurrent.Callable

/**
 * Providers a task for running npm with arguments.
 * By default It will be executed within the directory which is configured as the NPM home directory.
 *
 * @since 0.9.0
 */
@CompileStatic
class NpmCmdlineTask extends AbstractCmdlineTask {
    NpmCmdlineTask() {
        super()
        description = 'Runs npm with --args'
        commandProvider = { ->
            nodejs.resolvableNodeExecutable.executable.absolutePath
        }
        environmentProvider = project.provider({ ->
            nodejs.environment +
            [
                npm_config_userconfig  : npm.localConfig.absolutePath,
                npm_config_globalconfig: npm.globalConfig.absolutePath
            ]
        } as Callable<Map<String, Object>>)
        addArgs({ NpmExtension npm -> npm.resolvedNpmCliJs.executable.absolutePath }.curry(npm))
    }
}
