/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.npm

import groovy.json.JsonParserType
import groovy.json.JsonSlurper
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.apache.commons.collections4.map.ListOrderedMap
import org.ysb33r.grolifant.api.Version

import static groovy.json.JsonOutput.prettyPrint
import static groovy.json.JsonOutput.toJson
import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/** Parses a {@code package.json} file.
 *
 * <p> This is not a complete representation. It is only meant to provide enough information for the plugin
 * to make reasonable decisions about packages.
 *
 * @since 0.1
 */
@CompileStatic
@SuppressWarnings(['DuplicateStringLiteral'])
class PackageJson {

    static enum Indentation {
        TWO_SPACES,
        FOUR_SPACES
    }

    static class Element implements Comparable<Element> {
        Integer index = -1
        String key

        @Override
        int compareTo(Element rhs) {
            index <=> rhs.index
        }

        @Override
        String toString() {
            key
        }
    }

    public static final String PACKAGE_JSON = 'package.json'

    static Map<String, ?> parsePackageJsonToMap(final File packageJson) {
        Map<String, ?> map = [:]
        map.putAll(parsePackageJsonToGPath(packageJson) as Map<String, ?>)
        map
    }

    static Object parsePackageJsonToGPath(final File packageJson) {
        JsonSlurper parser = new JsonSlurper(type: (packageJson.size() > 4194304 ?
            JsonParserType.CHARACTER_SOURCE : JsonParserType.INDEX_OVERLAY))
        parser.parse(packageJson)
    }

    static PackageJson parsePackageJson(final File packageJson) {
        new PackageJson(parsePackageJsonToGPath(packageJson) as Map<String, ?>)
    }

    @SuppressWarnings('DuplicateStringLiteral')
    static String formatVersion(String ver, boolean forceSemver) {
        if (forceSemver) {
            if (ver == 'unspecified') {
                '0.0.0-alpha.0'
            } else {
                Version checkver = Version.of(stringize(ver))
                if (!checkver.minor || !checkver.patch) {
                    String semBaseVer = "${checkver.major}.${checkver.minor ?: '0'}.0"
                    if (checkver.milestone) {
                        "${semBaseVer}-m${checkver.milestoneSequence}"
                    } else if (checkver.beta) {
                        "${semBaseVer}-beta.${checkver.betaSequence}"
                    } else if (checkver.alpha) {
                        "${semBaseVer}-alpha.${checkver.alphaSequence}"
                    } else if (checkver.snapshot) {
                        "${semBaseVer}-alpha.${checkver.snapShotSequence ?: '0'}"
                    } else {
                        semBaseVer
                    }
                } else {
                    checkver.version
                }
            }
        } else {
            ver
        }
    }

    /** Formats a name suitable for {@code package.json}.
     *
     * @param name Proposed name
     * @return Package name
     *
     * @see https://docs.npmjs.com/files/package.json
     */
    static String formatName(String name) {
        String newName = name.toLowerCase(Locale.US)

        newName.size() > 214 ? newName[0..213] : newName
    }

    String getName() {
        get(NAME)
    }

    void setName(String nm) {
        set(NAME, nm)
    }

    String getVersion() {
        get(VERSION)
    }

    void setVersion(String ver) {
        set(VERSION, ver)
    }

    String getDescription() {
        get(DESCRIPTION)
    }

    String getLicense() {
        get(LICENSE)
    }

    String getHomepage() {
        get(HOMEPAGE)
    }

    List<String> getKeywords() {
        accessedList(KEYWORDS)
    }

    Map<String, String> getDependencies() {
        accessedMap(DEPS)
    }

    Map<String, String> getScripts() {
        accessedMap(SCRIPTS)
    }

    Map<String, String> getDevDependencies() {
        accessedMap(DEV_DEPS)
    }

    Map<String, String> getOptionalDependencies() {
        accessedMap(OPT_DEPS)
    }

    Map<String, String> getPeerDependencies() {
        accessedMap(PEER_DEPS)
    }

    Iterable<String> getBundledDependencies() {
        accessedList(BUNDLED_DEPS)
    }

    /** Returns all properties as map which will be suitable for writing out as JSON again.
     *
     * @return
     */
    Map<String, ?> asMap() {
        this.jsonMap as Map<String, ?>
    }

    /** Writes the contents to a {@code package.json} file
     *
     * @param destFile File path of which the filename should be {@code package.json}.
     * @param indent Whether to use two-space or four-space indentation
     * @param sortElements Whether to sort elements by the rules of
     * {@link https://github.com/keithamus/sort-package-json/blob/master/index.js}.
     */
    void writeToFile(File destFile, Indentation indent, boolean sortElements) {
        Map output = sortElements ? orderedMap : this.jsonMap

        if (indent == Indentation.FOUR_SPACES) {
            destFile.text = prettyPrint(toJson(output))
        } else {
            destFile.withWriter { sw ->
                prettyPrint(toJson(output)).eachLine {
                    sw.println(it.replaceFirst(~/^\s+/, UNINDENT))
                }
            }
        }
    }

    private PackageJson(Object jsonMap) {
        this.jsonMap = new ListOrderedMap<String, ?>()
        this.jsonMap.putAll(jsonMap as Map)
        1
    }

    private ListOrderedMap<String, ?> getOrderedMap() {
        def orderedMap = new ListOrderedMap<String, ?>()
        Map<String, ?> tmpMap = [:]
        tmpMap.putAll(this.jsonMap)

        SORT_PACKAGE_JSON.each { String name ->
            if (tmpMap.containsKey(name)) {
                orderedMap.put(name, tmpMap[name])
                tmpMap.remove(name)
            }
        }

        orderedMap.putAll(tmpMap)
        orderedMap
    }

    private String get(String name) {
        (String) jsonMap[name]
    }

    private void set(String name, Object value) {
        jsonMap.put(name, value)
    }

    @CompileDynamic
    @SuppressWarnings('UnnecessaryCast')
    private List<String> accessedList(String name) {
        if (!jsonMap[name]) {
            set(name, (List<String>) [])
        }
        (List<String>) jsonMap[name]
    }

    @CompileDynamic
    @SuppressWarnings('UnnecessaryCast')
    private Map<String, String> accessedMap(String name) {
        if (!jsonMap[name]) {
            set(name, [:] as Map<String, String>)
        }
        (Map<String, String>) jsonMap[name]
    }

    private static final String NAME = 'name'
    private static final String DESCRIPTION = 'description'
    private static final String VERSION = 'version'
    private static final String HOMEPAGE = 'homepage'
    private static final String LICENSE = 'license'
    private static final String KEYWORDS = 'keywords'
    private static final String DEPS = 'dependencies'
    private static final String DEV_DEPS = 'devDependencies'
    private static final String OPT_DEPS = 'optionalDependencies'
    private static final String PEER_DEPS = 'peerDependencies'
    private static final String BUNDLED_DEPS = 'bundledDependencies'
    private static final String SCRIPTS = 'scripts'

    private final ListOrderedMap<String, ?> jsonMap

    private static final List<String> SORT_PACKAGE_JSON = [
        NAME,
        VERSION,
        'private',
        DESCRIPTION,
        KEYWORDS,
        HOMEPAGE,
        'bugs', // TODO: sortObjectBy(['url', 'email'])
        'repository', // TODO: sort by url
        'funding', // TODO: sort by url
        LICENSE, // TODO: sort by url
        'author', // TODO: sort by people
        'sideEffects',
        'type',
        'exports', // TODO: sort
        'main',
        'jsdelivr',
        'unpkg',
        'module',
        'source',
        'browser',
        'types',
        'typings',
        'style',
        'example',
        'examplestyle',
        'assets',
        'bin', //TODO: sort
        'man',
        'directories', // TODO: sort
        'files',
        'workspaces',
        SCRIPTS, // TODO: sort
        'betterscripts', // TODO: sort
        'husky',
        'pre-commit',
        'commitlint', // TODO: sort
        'lint-staged', // TODO: sort
        'config',  // TODO: sort
        'nodemonConfig', // TODO: sort
        'browserify', // TODO: sort
        'xo',  // TODO: sort
        'prettier',
        'eslintConfig',
        'npmpkgjsonlint',
        'remarkConfig',
        'stylelint',
        'ava',
        'jest',
        'mocha',
        'nyc',
        'resolutions',
        DEPS,
        DEV_DEPS,
        PEER_DEPS,
        'peerDependenciesMeta',
        OPT_DEPS,
        BUNDLED_DEPS,
        'flat',
        'engines',
        'engineStrict',
        'os',
        'cpu',
        'preferGlobal',
        'publishCOnfig'
    ]
    // uniq: keywords, files

    private static final Closure UNINDENT = { String spaces ->
        ' ' * (spaces.size() / 2)
    }

}
