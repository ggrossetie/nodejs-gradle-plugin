/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.npm

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.NpmExecSpec
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.grolifant.api.v4.exec.ExecSpecInstantiator

/** Creates instances for {@link NpmExecSpec}
 *
 * @since 0.1
 */
@CompileStatic
class NpmExecSpecInstantiator implements ExecSpecInstantiator<NpmExecSpec> {

    NpmExecSpecInstantiator(NpmExtension npmExtension) {
        this.npmExtension = npmExtension
    }

    /** Instantiates a NPM execution specification.
     *
     * @param project Project that this execution specification will be associated with.
     * @return New NPM execution specification.
     */
    @Override
    NpmExecSpec create(Project project) {
        NpmExecSpec execSpec = new NpmExecSpec(project, npmExtension.resolver)
        execSpec.environment = NodeJSExecutor.defaultEnvironment
        execSpec
    }

    private final NpmExtension npmExtension
}
