/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.ysb33r.grolifant.api.OperatingSystem
import org.ysb33r.grolifant.api.v4.AbstractDistributionInstaller

import static org.ysb33r.grolifant.api.OperatingSystem.Arch.X86
import static org.ysb33r.grolifant.api.OperatingSystem.Arch.X86_64

/** Downloads specific versions of NodeJS.
 * Currently limited to Windows (x86, x86_64), MacOS, Linux (x86, x86_64).
 *
 * <p> There are more
 * binary packages are available from the NodeJS site, but currently these are not being tested not implemented.
 * This includes:
 *
 * <ul>
 *    <li> linux-armv6l.tar.xz
 *    <li> linux-armv7l.tar.xz
 *    <li> linux-arm64.tar.xz
 *    <li> linux-ppc64le.tar.xz
 *    <li> linux-ppc64.tar.xz
 *    <li> linux-s390x.tar.xz
 * </ul>
 * <p> (Patches welcome!)
 */
@CompileStatic
class Downloader extends AbstractDistributionInstaller {
    static final OperatingSystem OS = OperatingSystem.current()
    static final OperatingSystem.Arch ARCH = OS.arch
    static String baseURI = System.getProperty('org.ysb33r.gradle.nodejs.uri') ?: 'https://nodejs.org/dist'

    Downloader(final String version, final Project project) {
        super('nodejs', version, 'native-binaries/nodejs', project)
    }

    /** Provides an appropriate URI to download a specific tag of NodeJS.
     *
     * @param ver Version of Node to download
     * @return URI for a supported platform; {@code null} otherwise.
     */
    @Override
    URI uriFromVersion(final String ver) {
        String variant
        if (OS.windows) {
            if (OS.arch == X86) {
                variant = 'win-x86.zip'
            } else {
                variant = 'win-x64.zip'
            }
        } else if (OS.linux) {
            String arch
            switch (ARCH) {
                case X86_64:
                    arch = 'x64'
                    break
                case X86:
                    arch = 'x86'
                    break
            }
            if (arch) {
                variant = "linux-${arch}.tar.xz"
            }
        } else if (OS.macOsX) {
            variant = 'darwin-x64.tar.gz'
        }

        variant ? "${baseURI}/v${ver}/node-v${ver}-${variant}".toURI() : null
    }

    /** Returns the path to the {@code node} executable.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code node} or null if not a supported operating system.
     */
    @SuppressWarnings('UnnecessaryGetter')
    File getNodeExecutablePath() {
        File root = getDistributionRoot()
        if (root == null) {
            return null
        } else if (OS.windows) {
            new File(root, 'node.exe')
        } else {
            new File(root, 'bin/node')
        }
    }

    /** Returns the path to the included {@code npm-cli.js} Node.js executable JavaScript.
     * Will force a download if not already downloaded.
     *
     * @return Location of {@code npm-cli.js} or null if not a supported operating system.
     */
    @SuppressWarnings('UnnecessaryGetter')
    File getNpmExecutablePath() {
        File root = getDistributionRoot()
        if (root == null) {
            return null
        } else if (OS.windows) {
            new File(root, 'node_modules/npm/bin/npm-cli.js')
        } else {
            new File(root, '../lib/node_modules/npm/bin/npm-cli.js')
        }
    }

    @Override
    protected File getAndVerifyDistributionRoot(File distDir, String distributionDescription) {
        File dir = super.getAndVerifyDistributionRoot(distDir, distributionDescription)
        if (!IS_WINDOWS) {
            copyNpmAndNpxToBin(dir)
        }
        dir
    }

    private void copyNpmAndNpxToBin(File destDir) {
        project.logger.debug('Fixing npm & npx scripts in bin')
        project.copy { CopySpec spec ->
            spec.into new File(destDir, 'bin')
            spec.from new File(destDir, 'lib/node_modules/npm/bin/npx')
            spec.from new File(destDir, 'lib/node_modules/npm/bin/npm')
            spec.fileMode = 0755

            spec.filter { String line ->
                if (line.contains('$basedir/node_modules')) {
                    line.replace('node_modules', '../lib/node_modules')
                } else {
                    line
                }
            }
        }
    }
}

