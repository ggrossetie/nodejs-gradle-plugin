/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic

/** Describes a NPM package without a version
 *
 * @author Schalk W. Cronjé
 * @since 0.7
 */
@CompileStatic
class PackageDescriptor {
    final String name
    final String scope

    static PackageDescriptor of(final String name) {
        of(null, name)
    }

    static PackageDescriptor of(final String scope, final String name) {
        new PackageDescriptor(scope, name)
    }

    @Override
    String toString() {
        if (scope) {
            "@${scope}/${name}"
        } else {
            name
        }
    }

    private PackageDescriptor(final String scope, final String name) {
        this.name = name
        this.scope = scope
    }
}
