/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.nodejs.functionaltest.helper.DownloadTestSpecification
import org.ysb33r.gradle.nodejs.internal.Downloader
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable

class NodeJsExtensionSpec extends DownloadTestSpecification {
    Project project = ProjectBuilder.builder().build()

    void setup() {
        Downloader.baseURI = DownloadTestSpecification.NODEJS_CACHE_DIR.toURI()

        project.allprojects {
            apply plugin: 'org.ysb33r.nodejs.base'

            // tag::configure-with-version[]
            nodejs {
                executable version: DownloadTestSpecification.NODEJS_VERSION
            }
            // end::configure-with-version[]
        }
    }

    def 'Resolving by version should download NodeJS'() {
        setup:
        ResolvableExecutable resolver = project.nodejs.resolvableNodeExecutable

        when:
        File nodeJSPath = resolver.executable

        then:
        nodeJSPath != null
        nodeJSPath.absolutePath.startsWith(
            new File(project.gradle.startParameter.gradleUserHomeDir, 'native-binaries/nodejs').absolutePath
        )
    }
}