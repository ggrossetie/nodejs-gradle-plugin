/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmIntegrationTestSpecification

import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.PACKAGE_JSON

class SyncProjectToPackageJsonSpec extends NpmIntegrationTestSpecification {

    void 'Sync dependencies to package.json'() {
        setup:
        File packageJson = new File(projectDir, PACKAGE_JSON)
        def args = ['-i', '-s', 'syncPackageJson']
        buildFile << """
        version = '1.2.3'
        ext.antoraVersion = '2.2'
        dependencies {
            npm npmPackage(scope: 'antora', name: 'cli', tag: antoraVersion, type: 'dev')
            npm npmPackage(scope: 'antora', name: 'site-generator-default', tag: antoraVersion, type: 'dev')
        }

        task initPackageJson( type: org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInit ) {
            nodejs {
                useSystemPath()
            }
        }
        task syncPackageJson(type: org.ysb33r.gradle.nodejs.tasks.SyncProjectToPackageJson) {
            dependsOn initPackageJson
            forceSemver = false
            forceTwoSpaceIndent = true
            sortOutput = true
            packageJsonVersion = '1.2.4'
            packageJsonFile = initPackageJson.packageJsonFileProvider
            npmConfigurations 'npm'
        }
        """

        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, args).build()
        String contents = packageJson.text
        def matcher = contents =~ /(?s).+?\s+"name": "junit\d+?",\s+"version": "1.2.4",\s+"description": "".+/

        then:
        contents.contains('"@antora/cli": "2.2"')
        matcher.matches()
    }
}