/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.nodejs.functionaltest.helper.DownloadTestSpecification
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor

class NpmDependencySpec extends DownloadTestSpecification {

    Project project = ProjectBuilder.builder().build()

    void 'Declare a dependency and resolve it'() {
        given:
        project.allprojects {
            apply plugin: 'org.ysb33r.nodejs.npm'

            nodejs {
                executable version: DownloadTestSpecification.NODEJS_VERSION
            }

            // tag::declare-npm-dependency[]
            dependencies {
                npm npmPackage(name: 'stringz', tag: '0.2.2')
            }
            // end::declare-npm-dependency[]
        }

        when:
        NpmExecutor.initPkgJson(project, project.extensions.nodejs, project.extensions.npm)
        Set<File> files = project.configurations.getByName('npm').resolve()

        then:
        files.size() > 0
    }

    void 'Declare a dependency which requires a shell to install'() {
        def packageProperties = [name: 'puppeteer', tag: '1.15.0']

        project.allprojects {
            apply plugin: 'org.ysb33r.nodejs.npm'

            nodejs {
                executable version: NODEJS_VERSION
                useSystemPath()
            }

            dependencies {
                npm npmPackage(packageProperties)
            }
        }

        when:
        NpmExecutor.initPkgJson(project, project.extensions.nodejs, project.extensions.npm)
        Set<File> files = project.configurations.getByName('npm').resolve()

        then:
        files.size() > 0
    }
}