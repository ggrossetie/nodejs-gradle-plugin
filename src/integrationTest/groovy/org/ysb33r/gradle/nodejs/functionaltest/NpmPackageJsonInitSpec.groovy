/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmIntegrationTestSpecification
import spock.lang.Unroll

import static org.ysb33r.gradle.nodejs.internal.npm.PackageJson.PACKAGE_JSON

class NpmPackageJsonInitSpec extends NpmIntegrationTestSpecification {

    @Unroll
    void 'Initialise a package json file where location is #location'() {
        setup:
        File packageJson = useDefault ?
            new File(projectDir, PACKAGE_JSON) :
            new File(projectDir, "build/foo/${PACKAGE_JSON}")
        def args = ['-i', '-s', 'initPackageJson']
        buildFile << """
        version = '1.2.3'
        task initPackageJson( type: org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInit ) {
            nodejs {
                useSystemPath()
            }
            npm {
                ${homeDirectory}
            }
        }
        """

        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, args).build()

        then:
        packageJson.exists()
        packageJson.text.contains('"version": "1.2.3"')

        when:
        buildFile.text = buildFile.text.replace('1.2.3', '4.5.6')
        getGradleRunner(IS_GROOVY_DSL, projectDir, args).build()

        then:
        packageJson.text.contains('"version": "1.2.3"')

        where:
        location      | useDefault | homeDirectory
        'projectDir'  | true       | 'projectDir'
        'in buildDir' | false      | 'homeDirectory = new File(buildDir,"foo")'
    }
}

