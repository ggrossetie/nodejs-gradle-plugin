/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmIntegrationTestSpecification
import spock.lang.IgnoreIf
import spock.lang.Timeout
import spock.lang.Unroll
import spock.util.environment.RestoreSystemProperties

import static org.ysb33r.gradle.nodejs.plugins.WrapperPlugin.WRAPPER_TASK_NAME
import static org.ysb33r.gradle.nodejs.tasks.NodeBinariesCacheTask.PROPERTIES_FILENAME

@IgnoreIf({ SKIP_TESTS })
@RestoreSystemProperties
class NodeWrappersSpec extends NpmIntegrationTestSpecification {

    public static final List<String> ENV_VARS = OS.windows ? ['DEBUG=1', "TEMP=${System.getenv('TEMP')}"] : ['DEBUG=1']
    public static final Set<String> SCRIPTS = ['nodew', 'npmw', 'npxw']

    Map<String, File> wrappers = [:]

    void setup() {
        buildFile.text = '''
        plugins {
            id 'org.ysb33r.nodejs.wrapper'
        }
        '''

        SCRIPTS.each {
            wrappers.putAll([
                (it)                    : new File(projectDir, it),
                ("${it}_bat".toString()): new File(projectDir, "${it}.bat")
            ])
        }
    }

    @Timeout(300)
    void 'Create wrappers'() {
        when: 'The nodejs wrapper task is executed'
        BuildResult result = wrapperRunner.build()

        then: 'nodejs wrapper scripts are generated'
        result.task(":${WRAPPER_TASK_NAME}").outcome == TaskOutcome.SUCCESS
        wrappers.values()*.exists().every { it }
    }

    @Timeout(300)
    @Unroll
    void 'Run #wrapper.cmd'() {
        setup: 'wrappers have been created'
        wrapperRunner.build()

        when: 'The node wrapper script is executed'
        Result runResult = runWrapper(new File(projectDir, wrapper.cmd), wrapper.instruction)

        then: 'It should print out the help message'
        runResult.exitCode == wrapper.exit
        runResult.out.contains(wrapper.containedText)

        where:
        wrapper << [
            [
                cmd          : scriptName('nodew'),
                instruction  : '--help',
                containedText: 'NODE_DEBUG',
                exit         : 0
            ],
            [
                cmd          : scriptName('npmw'),
                instruction  : 'version',
                containedText: 'npm:',
                exit         : 0
            ],
            [
                cmd          : scriptName('npxw'),
                instruction  : '--help',
                containedText: 'npx --shell-auto-fallback',
                exit         : 0
            ]
        ]
    }

    static class Result {
        int exitCode
        String out
    }

    Result runWrapper(File wrapper, String args) {
        StringWriter err = new StringWriter()
        StringWriter out = new StringWriter()
        Process runWrapper = "${wrapper.absolutePath} ${args}".execute(ENV_VARS, wrapper.parentFile)
        runWrapper.consumeProcessOutput(out, err)
        runWrapper.waitForOrKill(1000)
        int exitCode = runWrapper.exitValue()
        if (exitCode) {
            println '---[ stderr ]----------------------------------'
            println err
            println '---[ stdout ]----------------------------------'
            println out
            println '---[ node-wrapper ]----------------------------'
            println new File(projectCacheDir, PROPERTIES_FILENAME).text
            println '-----------------------------------------------'
        }
        new Result(exitCode: exitCode, out: out.toString())
    }

    GradleRunner getWrapperRunner() {
        getGradleRunner(
            IS_GROOVY_DSL,
            projectDir,
            [
                'wrapper',
                WRAPPER_TASK_NAME,
                '-i',
                '-s'
            ]
        )
    }

    private static String scriptName(String baseName) {
        OS.windows ? "${baseName}.bat" : baseName
    }
}