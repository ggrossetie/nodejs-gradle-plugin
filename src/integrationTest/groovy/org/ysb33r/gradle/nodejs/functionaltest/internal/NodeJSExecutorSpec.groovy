/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest.internal

import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.GulpExtension
import org.ysb33r.gradle.nodejs.NodeJSExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmBaseTestSpecification
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor

class NodeJSExecutorSpec extends NpmBaseTestSpecification {

    void setup() {
        NpmExecutor.initPkgJson(project, project.extensions.nodejs, project.extensions.npm)
    }

    File setupGulpDownload(Project project) {
        project.allprojects {
            dependencies {
                npm npmPackage(name: 'gulp', tag: GulpExtension.GULP_DEFAULT)
            }
        }
        project.configurations.npm.resolve()
        NpmExtension npm = (NpmExtension) (project.extensions.npm)
        new File(npm.homeDirectory, 'node_modules/gulp/bin/gulp.js')
    }

    def 'Download a file then execute it'() {
        setup:
        File gulpFile = setupGulpDownload(project)
        OutputStream output = new ByteArrayOutputStream()
        NpmExtension npm = (NpmExtension) (project.extensions.npm)
        NodeJSExtension nodejs = (NodeJSExtension) (project.extensions.nodejs)

        when:
        NodeJSExecSpec execSpec = new NodeJSExecSpec(project, nodejs.resolver)
        execSpec.script gulpFile
        execSpec.scriptArgs '--help'
        execSpec.standardOutput output
        execSpec.workingDir npm.homeDirectory

        // there is no Gulpfile; we know it is going to fail.
        execSpec.ignoreExitValue true

        NodeJSExecutor.configureSpecFromExtensions(execSpec, nodejs)
        execSpec.environment NodeJSExecutor.defaultEnvironment

        project.allprojects {
            nodeexec execSpec
        }

        then:
        output.toString().contains('Manually set path of gulpfile')
    }

    def 'Run nodeexec via a closure'() {
        setup:
        setupGulpDownload(project)
        OutputStream output = new ByteArrayOutputStream()

        when:
        project.allprojects {
            // tag::nodeexec-with-closure[]
            nodeexec {
                script 'node_modules/gulp/bin/gulp.js' // <1>
                scriptArgs '--help' // <2>
                workingDir npm.homeDirectory // <3>
                executable nodejs.resolvableNodeExecutable // <4>
                // end::nodeexec-with-closure[]
                standardOutput output
                ignoreExitValue true
                // tag::nodeexec-with-closure[]
            }
            // end::nodeexec-with-closure[]
        }

        then:
        output.toString().contains('Manually set path of gulpfile')
    }

}