/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.gradle.testkit.runner.BuildResult
import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmIntegrationTestSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class CmdlineTaskSpec extends NpmIntegrationTestSpecification {

    void setup() {
        buildFile << """
            apply plugin : 'org.ysb33r.nodejs.cmdline'
        """
    }

    void 'Run node from command-line'() {
        when:
        BuildResult result = runTask('node', ['--args=-v --no-warnings'])

        then:
        result.task(':node').outcome == SUCCESS
        result.output.contains("v${NODEJS_VERSION}")
    }

    void 'Run npm from command-line'() {
        when:
        BuildResult result = runTask('npm', ['--args', 'version'])

        then:
        result.task(':npm').outcome == SUCCESS
        result.output.contains("node: '${NODEJS_VERSION}'")
    }

    void 'Run npx from command-line'() {
        when:
        BuildResult result = runTask('npx', ['--arg=--help'])

        then:
        result.task(':npx').outcome == SUCCESS
        result.output.contains('Execute binaries from npm packages')
    }

    BuildResult runTask(String taskName, List<String> taskArgs) {
        getGradleRunner(IS_GROOVY_DSL, projectDir, ['-i', '-s', taskName] + taskArgs).build()
    }
}