/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.functionaltest

import org.ysb33r.gradle.nodejs.functionaltest.helper.DownloadTestSpecification
import org.ysb33r.gradle.nodejs.functionaltest.helper.NpmIntegrationTestSpecification

import java.nio.file.Files
import java.nio.file.StandardCopyOption

class NpmPackageJsonInstallSpec extends NpmIntegrationTestSpecification {

    File packageJson

    void setup() {
        packageJson = new File(projectDir, 'package.json')
        Files.copy(
            new File(DownloadTestSpecification.RESOURCES_DIR, 'installtest-package.json').toPath(),
            packageJson.toPath(),
            StandardCopyOption.COPY_ATTRIBUTES
        )
    }

    // --------------------------------------------------------------------------------------------------------------
    // If this test fails check for an error in the logs such as
    //
    // "node-pre-gyp WARN Pre-built binaries not installable for bcrypt@3.0.8 and node@12.16.3 (node-v72 ABI, glibc)
    //   (falling back to source compile with node-gyp)"
    //
    //  Your next stop is https://github.com/kelektiv/node.bcrypt.js/releases.
    // Look in the assets for the release. If the package is there then you need to have a look in the debug log
    // itself. In the output will be a message like
    //
    //   "npm ERR!     /path/to/.npm/_logs/2020-05-03T21_31_49_074Z-debug.log"
    //
    // --------------------------------------------------------------------------------------------------------------
    def 'Install a set of dependencies from a package.json file'() {
        setup:
        String taskName = 'installFiles'
        buildFile << """
        task ${taskName}( type: org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInstall ) {
            nodejs {
                executable version : '12.14.1'  // Fails with 12.16.3
                useSystemPath() 
            }
        }
        """

        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, [taskName, '-s']).build()

        then:
        new File(projectDir, 'node_modules/brace-expansion').exists()
        new File(projectDir, 'node_modules/concat-map').exists()
    }
}

