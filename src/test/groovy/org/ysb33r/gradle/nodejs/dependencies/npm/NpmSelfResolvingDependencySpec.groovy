/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.dependencies.npm

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import spock.lang.Specification

class NpmSelfResolvingDependencySpec extends Specification {
    Project project = ProjectBuilder.builder().build()

    void 'Can copy a dependency'() {
        project.apply plugin: 'org.ysb33r.nodejs.npm'
        NodeJSExtension nodejs = project.extensions.nodejs
        NpmExtension npm = project.extensions.npm

        given:
        def props = [
            scope         : 'foo',
            name          : 'apackage',
            tag           : '1.0',
            'install-args': ['-a', '-b'],
            path          : 'foo/bar'
        ]

        def original = new NpmSelfResolvingDependency(project, nodejs, npm, props)

        when:
        def copy = (NpmSelfResolvingDependency) original.copy()

        then:
        verifyAll {
            original.scope == copy.scope
            original.name == copy.name
            original.tagName == copy.tagName
            original.installArgs == copy.installArgs
            original.path == copy.path
        }
    }
}