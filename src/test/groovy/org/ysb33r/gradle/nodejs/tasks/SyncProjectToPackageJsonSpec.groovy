/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import org.ysb33r.gradle.nodejs.helper.UnittestBaseSpecification
import spock.lang.Unroll

class SyncProjectToPackageJsonSpec extends UnittestBaseSpecification {

    @Unroll
    void 'Versions will be semver by default: #ver -> #semver'() {
        setup:
        String taskName = 'sync'
        SyncProjectToPackageJson sync = project.tasks.create(taskName, SyncProjectToPackageJson)

        when:
        sync.packageJsonVersion = ver

        then:
        sync.packageJsonVersion.get() == semver

        where:
        ver            | semver
        '1.0'          | '1.0.0'
        '2.3.4'        | '2.3.4'
        '1.0-alpha1'   | '1.0.0-alpha.1'
        '1.0.0-alpha1' | '1.0.0-alpha.1'
    }

    @Unroll
    void 'If semver is not set then original version is used'() {
        setup:
        String taskName = 'sync'
        SyncProjectToPackageJson sync = project.tasks.create(taskName, SyncProjectToPackageJson)
        sync.forceSemver = false

        when:
        sync.packageJsonVersion = ver

        then:
        sync.packageJsonVersion.get() == ver
        where:
        ver << [ '1.0', '1.0.0']
    }

}