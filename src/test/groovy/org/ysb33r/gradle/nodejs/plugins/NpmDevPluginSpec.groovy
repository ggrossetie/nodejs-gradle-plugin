/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2017-2020
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import org.gradle.api.tasks.Delete
import org.ysb33r.gradle.nodejs.helper.UnittestBaseSpecification
import org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInstall
import org.ysb33r.gradle.nodejs.tasks.SyncProjectToPackageJson

class NpmDevPluginSpec extends UnittestBaseSpecification {

    void 'Development plugin can be applied'() {
        when:
        project.apply plugin: NpmDevPlugin

        then:
        project.tasks.getByName('packageJsonInstall') instanceof NpmPackageJsonInstall
        project.tasks.getByName('cleanPackageJsonInstall') instanceof Delete
        project.tasks.getByName('syncProjectToPackageJson') instanceof SyncProjectToPackageJson
    }
}