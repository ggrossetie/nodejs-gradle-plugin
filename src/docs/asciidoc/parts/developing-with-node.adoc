== Using Gradle to Manage Javascript Projects
[[nodedev]]

Using Gradle to manage full Node.js project development might seem strange to many, but there is actually a great synergy. Gradle can be used to manage the whole ecosystem around the project including:

* Bootstrapping tools.
* Managing infrastructure.
* Controlling pipelines and deployment.
* Consolidating reports.
* Managing releases.
* One of more JavaScript projects can be managed as part of a much larger multi-project build.

Even-though there are a vast range of tools available in the NPM ecosystem, tying everything together can be painful. If this this done correctly Gradle can be transparent to most of your Javascript development team.

=== Setting it up

Start by adding an empty `build.gradle`  file as well as your `settings.gradle` file. Remember to add a wrapper as well.

.Directory layout
[listing]
----
├── build.gradle
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradle.properties
├── gradlew
├── gradlew.bat
├── settings.gradle
└── src
    └── node  <1>
        ├── app
        ├── tests
        ├── node_modules
        ├── package.json
        └── package-lock.json
----
<1> Place all of your Javascript code in `src/node` including files such as `.editorconfig`, `.eslintrc.js` etc. In this way your pure Javascript developer can simply point their editor to this folder after checkout.


Edit `build.gradle` to add the necessary plugin(s).

.build.gradle
[source,groovy]
[subs="attributes+"]
----
plugins {
  id 'org.ysb33r.nodejs.dev' version '{revnumber}' // <1>
  id 'org.ysb33r.nodejs.wrapper' version '{revnumber}' // <2>
}
----
<1> The `dev` plugin provides all of the necessary conventions for marrying Javascript development with Gradle management.
<2> The use of the `wrapper` plugin is not required, but is recommended especially for IDE integration.

Apply the `org.ysb33r.nodejs.dev` plugin will:
* Set up `npm.homeDirectory` to point to `src/node`
* Add `packageJsonInstall` and `syncProjectToPackageJson` tasks
* Add rules to run NPM scripts defined in the `scripts` block of `package.json`.

=== Running scripts

If you have any scripts defined in `package.json` you can run them from Gradle by simply doing `npmRunTest` (for a script called `test`).

If you need to configure the specific task to pass parameters, then you can add them

[source,groovy]
----
npmRunTest {
   cmdArgs '--grep=pattern' // <1>
}
----
<1> The arguments will be passed to the script after `--` has been passed. Thus this example is the equivalent of ++npm run test -- --grep=pattern++.

=== Managing Lifecycle in Node.js Projects

Running the `assemble` task will
* Ensure that the project's version is synchronised to the version in `package.json`. It will also perform adjustments to ensure that the version conforms to semantic versioning.
* Install all necessary packages via NPM.

=== Controlling package.json format

When updating the `package.json file, rewriting might conflict with the coding standards of some teams. For this reason `syncProjectToPackageJson` offers some configuration possibilities.

[source,groovy]
----
syncProjectToPackageJson {
  forceTwoSpaceIndent = true // <1>
  sortOutput = true // <2>
}
----
<1> Force a two-space identation when writing out. Default `false`.
<2> Sorts the elements in a similar way to the https://www.npmjs.com/package/sort-package-json[sort-package-json] NPM package.