== Working with package.json

There are a number of tasks that work directly with `package.json`.

=== Creating a package.json file

link:{groovydoc}/tasks/NpmPackageJsonInit.html[NpmPackageJsonInit] will create a bare bones `package.json` file

[source,groovy]
----
task packageJsonInit(type: org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInit) {
   npm {
       homeDirectory = 'src/node' // <1>
   }
}
----
<1> Sets location of `package.json`. If not set it will default to `${npm.homeDirectory}/package.json`.

=== Installing NPM packages

link:{groovydoc}/tasks/NpmPackageJsonInstall.html[NpmPackageJsonInstall] installs packages defined in `package.json`.

[source,groovy]
----
task packageJsonInstall(type: org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInstall) {
   npm {
       homeDirectory = 'src/node' // <1>
   }
}
----
<1> Sets location of `package.json`. If not set, it will default to `${project.npm.homeDirectory}/package.json`.

=== Synchronising project version to package.json

link:{groovydoc}/tasks/SyncVersionToPackageJson.html[SyncVersionToPackageJson] allows for the project version to be synchronised.

[source,groovy]
----
task syncPackageJson(type: org.ysb33r.gradle.nodejs.tasks.SyncVersionToPackageJson) {
   forceSemver = true // <1>
   packageJsonVersion = '1.2.3' // <2>
   packageJsonFile = 'src/node/package.json' // <3>
   npmConfigurations 'npm' // <4>
   conflictMode = GRADLE // <5>
}
----
<1> Decide whether the project version should be converted to a https://docs.npmjs.com/misc/semver[semver] version that is NPM compatible. By default this is set to `true` and if the version it `1.0` then the version written will be `1.0.0`.
<2> Set the version to be written. If not set, the `project.version` will be used by default. The version is lazy-evaluated and can be anything that {stringize}[stringize] will convert.
<3> Set the location, which is lazy-evaluated and can be anything that {fileize}[fileize] will convert This setting is required.
<4> If you want to sync dependencies from Gradle into `package.json` you can add those configurations in here. See <<DependencyManagement,Dependency Management>> for more details on specifying NPM packages in Gradle configurations/
<5> If you are providing packages via GRadle you need to say which source to use when there are conflicting package versions. Valid values are `GRADLE` and `PACKAGE_JSON`.

